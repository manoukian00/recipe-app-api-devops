terraform {
  backend "s3" {
    bucket         = "recepie-app-api-tfstate"
    key            = "recepie-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recepie-app-api-devops-tfstate-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}


locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Enviroenment = terraform.workspace
    Project      = var.project
    Owner        = var.contact
    ManagedBy    = "Terraform"
  }
}