variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recepie-app-api-devops"
}

variable "contact" {
  default = "test@mail.com"
}

variable "server_type" {
  default = "t2.micro"
}
